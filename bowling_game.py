class BowlingGame:
    def __init__(self):
        self.rolls = []

    def roll(self, pins):
        self.rolls.append(pins)

    def score(self):
        total = 0
        for first in range(0, len(self.rolls), 2):
            if self.rolls[first] + self.rolls[first+1] == 10:
                total += 10 + self.rolls[first+2]
            else:
                total += self.rolls[first] + self.rolls[first+1]
        return total
    
    
