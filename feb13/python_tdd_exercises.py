def reverse_list(lst):
    """
    Reverses order of elements in list lst.
    """
    reversed = []
    for item in lst:
        reversed = [item] + reversed
    return reversed

def reverse_string(s):
    """
    Reverses order of characters in string s.
    """
    f = ""
    for letter in s:
        f = str(letter) + str(f)
    return str(f)

def is_english_vowel(c):
    """
    Returns True if c is an english vowel
    and False otherwise.
    """
    f = ["a", "e", "i", "o", "u", "y", "A", "E", "I", "O", "U", "Y"]
    if c in f:
        return True

def count_num_vowels(s):
    """
    Returns the number of vowels in a string s.
    """
    y = 0
    f = ["a", "e", "i", "o", "u", "y", "A", "E", "I", "O", "U", "Y"]
    for c in s:
        if c in f:
            y += 1
    return y

def histogram(l):
    """
    Converts a list of integers into a simple string histogram.
    """
    f = ""
    s = ""
    for item in l:
        for i in range(item):
            f += "#"
        if len(f) == int(item):
            s = f + '\n' + f + f + '#' +  '\n' + '#'
    return s

def get_word_lengths(s):
    """
    Returns a list of integers representing
    the word lengths in string s.
    """
    f = []
    split = s.split()
    for i in split:
        f.append(len(i))
    return f

def find_longest_word(s):
    """
    Returns the longest word in string s.
    In case there are several, return the first.
    """
    f = ""
    split = s.split()
    for i in split:
        if len(i) > len(f):
            f = i
        else:
            f = f


    return f

def validate_dna(s):
    """
    Return True if the DNA string only contains characters
    a, c, t, or g (lower or uppercase). False otherwise.
    """
    adn = ["a", "c", "t", "g", "A", "C", "T", "G"]
    for i in s:
        if i in adn:
            s += i
        else:
            return False
    return s
def base_pair(c):
    """
    Return the corresponding character (lowercase)
    of the base pair. If the base is not recognized,
    return 'unknown'.
    """
    # dna_pair = {"a":"t", "A":"t", "a":"T", "A":"T", "g":"c", "G":"c", "g":"C",
    dna_pair = {"a":"t", "g":"c", "t":"a", "c":"g"} 
    #A = ["a", "A"]
    #T = ["t", "T"]
    #C = ["c", "C"]
    #G = ["g", "G"]
    c = c.lower()           # I kinda of cheated here. It makes the test pass
                            # though
    if c in dna_pair:
        return dna_pair[c]
    else:
        return "unknown"

def transcribe_dna_to_rna(s):
    """
    Return string s with each letter T replaced by U.
    Result is always uppercase.
    """
    Pal = ["T", "t"]
    rna = ""
    for i in s:
        if i in Pal:
            i = "U"
        rna += i
        rna = rna.upper()
    return rna

def get_complement(s):
    """
    Return the DNA complement in uppercase
    (A -> T, T-> A, C -> G, G-> C).
    """
    DNA = ""
    for i in s:
        if i == 't' or i == 'T':
            i = 'A'
        elif i == 'a' or i == 'A':
            i = 'T' 
        elif i == 'c' or i == 'C':
            i = 'G'
        elif i == 'g' or i == 'G':
            i = 'C'
        DNA += i
    return DNA

def get_reverse_complement(s):
    """
    Return the reverse complement of string s
    (complement reversed in order).
    """
    DNA = ""
    for i in s:
        if i == 't' or i == 'T':
            i = 'A'
        elif i == 'a' or i == 'A':
            i = 'T' 
        elif i == 'c' or i == 'C':
            i = 'G'
        elif i == 'g' or i == 'G':
            i = 'C'
        DNA = i + DNA 
    return DNA

def remove_substring(substring, string):
    """
    Returns string with all occurrences of substring removed.
    """
    return(string.replace(substring, ""))

def get_position_indices(triplet, dna):
    """
    Returns list of position indices for a specific triplet (3-mer)
    in a DNA sequence. We start counting from 0
    and jump by 3 characters from one position to the next.
    """
    count = 0
    poss = []
    thee = [(dna[i:i+3]) for i in range(0,len(dna),3)]
    for this in thee:
        if this == triplet:
            poss.append(count)
        count += 1
    return poss
