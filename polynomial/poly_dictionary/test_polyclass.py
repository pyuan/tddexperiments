from unittest import TestCase

from polyclass import Polynomial

class test_polyclass(TestCase):

    #def test_string_output(self):
    #    self.assertEqual( 
    #    {4: 3.0, 3: 2.0, 2: -8.0, 1: 14.0, 0: 12.0}, "3.0x^4+2.0x^3-8x^2+14x+12"
    #    )
    #def setUp(self):
       #self.poly = Polynomial() 
    def setUp(self):
        self.poly = Polynomial('3x^2+9x-6')

    def test_easy_output_of_write(self):
        self.assertEqual(self.poly.__str__(), '3x^2+9x-6') 
