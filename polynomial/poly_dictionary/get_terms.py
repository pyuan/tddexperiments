import re

def extract_poly_val(polystr):
    regex = re.compile(r"([+-]*)(\d+)?(x(\^(\d+))?)?")
    result = regex.findall(polystr)
    result.pop() #gets rid of empty tuple
    terms = {}
    for i in range(len(result)):
        if result[i][2] == "x": #handles term power of 1 
            terms[1] = float(result[i][0] + result[i][1])
        elif result[i][2] == "": #handles term power of 0
            terms[0] =  float(result[i][0] + result[i][1])
        else: #handles everything else
            terms[int(result[i][4])] = float(result[i][0] + result[i][1])
    return(terms)
    
