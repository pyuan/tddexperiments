import re

def extract_poly_val(polystr):
    """
      >>> extract_poly_val("2x^3-6x+3")
      {3: 2.0, 1: -6.0, 0: 3.0}
    """
    regex = re.compile(r"([+-]*)(\d+)?(x(\^(\d+))?)?")
    result = regex.findall(polystr)
    result.pop() #gets rid of empty tuple
    terms = {}
    for i in range(len(result)):
        if result[i][2] == "x": #handles term power of 1 
            terms[1] = float(result[i][0] + result[i][1])
        elif result[i][2] == "": #handles term power of 0
            terms[0] =  float(result[i][0] + result[i][1])
        else: #handles everything else
            terms[int(result[i][4])] = float(result[i][0] + result[i][1])
    return(terms)
    
class Polynomial:
    def __init__(self, terms):
        self.terms = extract_poly_val(terms)

    def __str__(self):
        """
          >>> p = Polynomial("3x^4+2x^2-6")
          >>> print(p)
          3x^4+2x^2-6
        """
        polyform = ""
        for key in self.terms:        
            if key == 0:
                polyform += str(self.terms[0])
        return polyform


if __name__ == "__main__":
    import doctest
    doctest.testmod()
