polystr_1 = {4: 3.0, 3: 2.0, 2: -8.0, 1: 14.0, 0: 12.0}
polystr_2 = {5:1.0, 4: 5.0, 2:1.0, 1:14.0, 0:9.0}

a = polystr_1.keys()
b = polystr_2.keys()

c = set(a).intersection(b)
new_dict ={}

for i in c:
    new_dict[i] = polystr_1[i] + polystr_2[i]

d = a ^ b

for i in d:
    if i in polystr_1:
        new_dict[i] = polystr_1[i]
    else:
        new_dict[i] = polystr_2[i]

{k: v for v, k in sorted(new_dict.items(), key=lambda item: item[1])}
print(new_dict)
