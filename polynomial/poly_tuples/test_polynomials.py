from unittest import TestCase

from polynomial import Polynomial

class PolynomialTestSuite(TestCase):

   #def test_create_polynomial(self):
   #    self.p1 = Polynomial('2x^2 + 2x + 2')
   #    self.assertEqual(str(self.p1), '2x^2+2x+2')

   #def test_create_polynomial_without_spaces_between_terms(self):
   #    self.p2 = Polynomial('7x^3 + 4x^2 + x')
   #    self.assertEqual(str(self.p2), '7x^3+4x^2+x')

    def test_find_coeff_and_exp(self):
        self.p1 = Polynomial('3x^4 + 7x^2')
        self.assertEqual(Polynomial.get_terms(self.p1) , [(3, 4), (7, 2)])

    def test_new_test(self):
        self.p1 = Polynomial('10x^5 + 13x^2 - 8x - 44')
        self.assertEqual(Polynomial.get_terms(self.p1) , [(10, 5), (13, 2), 
        (-8, 1), (-44, 0)])

