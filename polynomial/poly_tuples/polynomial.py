import re

class Polynomial:
    def __init__(self, poly_str):
        self.poly_str = poly_str
        self.get_terms()
        
    def get_terms(self):
        self.poly_str  = (self.poly_str.replace(' ', ''))
        regex = re.compile(r"([+-]*)(\d+)?(x(\^(\d+))?)?")
        terms = regex.findall(self.poly_str)
        terms.pop()
        self.results = []
        for term in terms:
            if term[1] is '':
                coeff = 1
            else:
                coeff = int(term[1])
            if term[0] == '-':
                    coeff = -coeff
            if term[2] is '':
                exp = 0
            elif term[4] is '':
                exp = 1
            else:
                exp = int(term[4])

            tup = (coeff, exp)
            self.results.append(tup)
        return(self.results)
   

